﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Animator))]
public class PlayerController : MonoBehaviour
{
    public static PlayerController playerInstance; //instanca ovog objekta

    //Public konstante dostupne unutar inspectora za modifikaciju
    public float speed; // Konstanta za brzinu kretnje 
    public float jumpForce; // Konstanta za jačinu skoka
    public LayerMask groundAndEnemyLayer; // Sloj na kojem se zemlja nalazi-- odabrati "Ground" unutar Inspectora
    public float groundCheckDistance = 0.6f; // udaljenost na kojoj detektira Slojeve "Enemy" i "Ground"

    //Player stats
    private int currentHealth = 5; // zdravlje igrača
    public int maxHealth = 5;

    //FireBall Attack
    public GameObject fireball; // referenca na FireBall objekt 
    public float _lastMoveH; //varizablja za pamćenje strane na koju je okrenut igrač 
    public float fireBallAttackColldown = 1f; // Vrijeme čekanja između svakog "FireBall" napada
    public int fireBallDamage = 2; // Šteta što fireBall napad nanosi 
    public float animationFireballRate = 0.4f; //dužina animacije 
    private float _nextFire = 0.0f; //varijabla za pamćenje vremena sljedećeg napada
    private float _animationFireBallNext = 0.0f; // varijabla za pamćenje vremena za "FireBall" animacije

    //Komponente
    private Rigidbody2D _thisRigidbody;
    private Animator _anim;
    private SpriteRenderer _playerSprite;

    public AudioSource _sfxAudioController;

    public AudioClip jumpAudioClip;
    public AudioClip takeDamageAudioClip;
    public AudioClip fireBallAudioClip;
    //Varijable za animaciju
    private bool _moving;
    private bool _jumping;
    private bool _fireBallFired;


    //UI
    public Text healthText;


    //Broj novčića pokupljeno 
    private int _pickedUpCoins;

    public int PickedUpCoins
    {
        get
        {
            return _pickedUpCoins;
        }

        set
        {
            _pickedUpCoins = value;
        }
    }


    /// <summary>
    /// Početna Unity metoda koja se izvršava prije ostalih
    /// Unutar metode dohvaćamo ostale komponente
    /// </summary>
    private void Awake()
    {
        if (playerInstance != null)
        {
            return;
        }
        else
        {
            playerInstance = this;
        }

        _thisRigidbody = GetComponent<Rigidbody2D>();
        _anim = GetComponent<Animator>();
        _playerSprite = GetComponent<SpriteRenderer>();
        _sfxAudioController = GetComponent<AudioSource>();
        _lastMoveH = 1f;
        currentHealth = maxHealth;
        healthText.text = currentHealth + "/" + maxHealth;
    }

    /// <summary>
    /// Metoda koje se zove više puta unutar jedne sekunde
    /// </summary>
    void FixedUpdate()
    {
        if (_animationFireBallNext < Time.time)
        {
            _fireBallFired = false;
            _anim.SetBool("FireBall", _fireBallFired);
        }
        PlayerControl();
        CheckDead();
        AnimateMovement();
        Debug.Log("Trenutni Score:" + PickedUpCoins);
        healthText.text = currentHealth + "/" + maxHealth;

    }

    /// <summary>
    /// Kretanje igrača uključujući horizontalne kretnje i vertikalne (skakanje)
    /// </summary>
    private void PlayerControl()
    {
        _moving = false;

        if (Input.GetAxis("Horizontal") != 0)
        {
            _thisRigidbody.velocity = new Vector2(Input.GetAxis("Horizontal") * speed, _thisRigidbody.velocity.y);
            if (Input.GetAxis("Horizontal") > 0)
            {
                _playerSprite.flipX = false;
                _lastMoveH = 1f;
            }


            else if (Input.GetAxis("Horizontal") < 0)
            {
                _playerSprite.flipX = true;
                _lastMoveH = -1f;
            }
            _moving = true;

        }
        if (_jumping)
        {
            RaycastHit2D hit2D = Physics2D.Raycast(_thisRigidbody.position, Vector2.down, groundCheckDistance, groundAndEnemyLayer);
            Debug.DrawRay(_thisRigidbody.position, Vector2.down * groundCheckDistance, Color.blue, 2f);


            if (hit2D)
            {
                _jumping = false;
                Debug.Log("Hit");
                Debug.Log("Falling :" + _thisRigidbody.velocity.y);
            }
        }

        if (!_jumping && Input.GetKey(KeyCode.W))
        {
            _sfxAudioController.PlayOneShot(jumpAudioClip);
            _thisRigidbody.velocity = new Vector2(_thisRigidbody.velocity.x, jumpForce);
            _jumping = true;
            Debug.Log("Jumping :" + _thisRigidbody.velocity.y);
        }
        if (Input.GetKey(KeyCode.F) && Time.time > _nextFire)
        {
            _nextFire = Time.time + fireBallAttackColldown;
            _animationFireBallNext = Time.time + animationFireballRate;
            _fireBallFired = true;
            FireBallAttack();

        }



    }
    private void FireBallAttack()
    {
        if (_fireBallFired)
        {
            _sfxAudioController.PlayOneShot(fireBallAudioClip);
            _anim.SetBool("FireBall", _fireBallFired);
            Vector2 fireBallPosition = this.transform.position;
            fireBallPosition += new Vector2(_lastMoveH, 0);
            Instantiate(fireball, fireBallPosition, Quaternion.Euler(Vector3.zero));

        }

    }

    //Oduzima zdravlje za određeni "damage"
    public void TakeDamage(int damage)
    {
        if (currentHealth != 0)
        {
            _sfxAudioController.PlayOneShot(takeDamageAudioClip);
            currentHealth -= damage;
            Debug.Log("Trenutni HP:" + currentHealth);
        }
    }
    //Provjerava da li je zdravlje došlo do 0 
    private void CheckDead()
    {
        if (currentHealth <= 0)
        {
            Destroy(this.gameObject);
        }
    }

    /// <summary>
    /// Postavljanje animacije za skakanje i za kretanje
    /// </summary>
    private void AnimateMovement()
    {
        _anim.SetBool("Moving", _moving);
        _anim.SetBool("Jumping", _jumping);
    }


}
