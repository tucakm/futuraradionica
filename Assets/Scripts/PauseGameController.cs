﻿using UnityEngine;
//Klasa za pauziranje igre
public class PauseGameController : MonoBehaviour
{

    public bool isPaused = false; // prikaz stanja

    public GameObject PauseUI; // referenca na UI



    //Update funkcija se izvodi konstantno
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!isPaused)
            {
                PauseGame();

            }
            else
            {
                ResumeGame();
            }
        }
    }
    //Pauzira igru
    private void PauseGame()
    {
        PauseUI.SetActive(true);
        Time.timeScale = 0f; // kada je vrijeme 0 svi objekti unutar igre kontrolirani pomoću rigidbody se ne mogu kretati
        isPaused = true;
    }
    //Nastavak igre
    private void ResumeGame()
    {
        PauseUI.SetActive(false);
        Time.timeScale = 1f; //1 označava početno stanje vremena
        isPaused = false;
    }
}
