﻿using UnityEngine;

public class CameraController : MonoBehaviour
{

    private Vector2 velocity;
    public float smoothX;




    void Start()
    {

    }

    void FixedUpdate()
    {
        if (PlayerController.playerInstance != null)
        {
            float posX = Mathf.SmoothDamp(this.transform.position.x, PlayerController.playerInstance.transform.position.x, ref velocity.x, smoothX);
            this.transform.position = new Vector3(posX, this.transform.position.y, this.transform.position.z);
        }
    }
}
