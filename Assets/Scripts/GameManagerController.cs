﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class GameManagerController : MonoBehaviour
{

    GameObject[] coins;
    public bool isPaused = false; // prikaz stanja
    public GameObject PauseUI; // referenca na UI
    //UI
    public Text CoinsText;
    public GameObject endScreen;
    public Text endGameText;
    void Start()
    {
        coins = GameObject.FindGameObjectsWithTag("PickUpCoin");
    }
    void Update()
    {
        if (SceneManager.GetActiveScene().name == "FirstScene")
        {
            if (PlayerController.playerInstance != null)
            {
                CoinsText.text = PlayerController.playerInstance.PickedUpCoins + "/" + coins.Length;
                if (PlayerController.playerInstance.PickedUpCoins == coins.Length)
                {
                    EndScreen();
                }
            }
            else
            {
                GameOver();
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (!isPaused)
                {
                    PauseGame();

                }
                else
                {
                    ResumeGame();
                }
            }
        }
    }
    public void Quit()
    {
        Application.Quit();
    }
    public void EndScreen()
    {
        endGameText.text = "Congratulations";
        endScreen.SetActive(true);

    }
    public void Restart()
    {

        SceneManager.LoadScene("FirstScene");

    }
    public void GameOver()
    {
        endGameText.text = "Game Over";
        endScreen.SetActive(true);
    }

    private void PauseGame()
    {
        PauseUI.SetActive(true);
        Time.timeScale = 0f; // kada je vrijeme 0 svi objekti unutar igre kontrolirani pomoću rigidbody se ne mogu kretati
        isPaused = true;
    }
    //Nastavak igre
    private void ResumeGame()
    {
        PauseUI.SetActive(false);
        Time.timeScale = 1f; //1 označava početno stanje vremena
        isPaused = false;
    }

}
