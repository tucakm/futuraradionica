﻿using UnityEngine;


[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Animator))]
public class EnemyController : MonoBehaviour
{

    public float speed; //brzina kretnje neprijatelja 
    public int attackDamage = 1; // udar prilikom kolizije
    public Transform groundDetection;
    public float groundDetectDistance = 0.5f;
    public float wallDetectDistance = 0.3f;
    public LayerMask wallAndEnemyDetect;
    public LayerMask playerDetect;
    public float checkDistance = 5f;
    public int currentHealth = 5;
    private bool _movingLeft;

    //Audio 
    private AudioSource _sfxEnemyAudio;
    public AudioClip enemyTakeDamageAudio;

    void Awake()
    {
        _movingLeft = true;
        _sfxEnemyAudio = GetComponent<AudioSource>();
    }

    void Update()
    {
        EnemyControll();
        CheckDead();

    }
    private void EnemyControll()
    {

        RaycastHit2D groundDetected = Physics2D.Raycast(groundDetection.position, Vector2.down, groundDetectDistance);
        RaycastHit2D horizontalDetect = Physics2D.Raycast(groundDetection.position, Vector2.left, wallDetectDistance);
        RaycastHit2D playerDetected = Physics2D.CircleCast(this.transform.position, checkDistance, Vector2.zero, checkDistance, playerDetect);



        if (!playerDetected)
        {
            transform.Translate(Vector2.left * speed * Time.deltaTime);
        }

        if (playerDetected)
        {
            transform.position = Vector2.MoveTowards(this.transform.position, PlayerController.playerInstance.transform.position, 2 * speed * Time.deltaTime);
        }

        else if (groundDetected.collider == null || horizontalDetect)
        {
            if (_movingLeft)
            {
                transform.eulerAngles = new Vector3(0, 0, 0);
                _movingLeft = false;
            }
            else
            {
                transform.eulerAngles = new Vector3(0, 180, 0);
                _movingLeft = true;
            }
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            PlayerController.playerInstance.TakeDamage(attackDamage);

        }
    }
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(this.transform.position, checkDistance);
    }
    public void TakeDamage(int damage)
    {
        if (currentHealth != 0)
        {
            _sfxEnemyAudio.PlayOneShot(enemyTakeDamageAudio);
            currentHealth -= damage;
            Debug.Log("Enemy trenutni HP:" + currentHealth);
        }
    }
    private void CheckDead()
    {
        if (currentHealth <= 0)
        {
            Destroy(this.gameObject);
        }
    }
}
