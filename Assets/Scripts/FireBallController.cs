﻿using UnityEngine;
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Animator))]
public class FireBallController : MonoBehaviour
{

    public float speed;
    private Rigidbody2D _thisRigidbody2D;
    private Animator _anim;
    private float _flipMovePosition;
    void Awake()
    {
        _thisRigidbody2D = GetComponent<Rigidbody2D>();
        _anim = GetComponent<Animator>();
        _flipMovePosition = PlayerController.playerInstance._lastMoveH;
    }


    void FixedUpdate()
    {

        _thisRigidbody2D.velocity = new Vector2(_flipMovePosition * speed, 0);
        Destroy(this.gameObject, 6f);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
        {
            collision.gameObject.GetComponent<EnemyController>().TakeDamage(PlayerController.playerInstance.fireBallDamage);
            DeadStatus();

        }
        else if (collision.tag != "PickUpCoin")
        {
            DeadStatus();
        }
    }
    private void DeadStatus()
    {
        _thisRigidbody2D.constraints = RigidbodyConstraints2D.FreezeAll;
        _anim.SetBool("FireBallDead", true);
        Destroy(this.gameObject, 0.4f);
    }

}
