﻿using UnityEngine;

public class CoinPickUpController : MonoBehaviour
{
    public AudioSource _sfxAudioCoinPickup;
    public AudioClip coinPickUpAudio;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            _sfxAudioCoinPickup.PlayOneShot(coinPickUpAudio);
            gameObject.SetActive(false);
            PlayerController.playerInstance.PickedUpCoins += 1;
        }
    }
}
